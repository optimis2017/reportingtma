$(document).ready(function () {

var monYear;
var barchart;
var diagC;
//regroupe les requétes SQL
function regroupSql(){
  visible();
  sql();
  sqlTicketAgent();
  sqlTmaAgent();
  sqlTicketOuvertAgent();
}

function sql(){
  $.ajax({
      url: '../model/queryTMA.php', // La ressource ciblée
      type: 'GET', // Le type de la requête HTTP.
      data: {year: +monYear},
      dataType: 'json', // Le type de données à recevoir
      success: function (data, statut) { // success est toujours en place, bien sûr !
        console.log(data, statut);
         if (data) {
              datasToDisplaySocietes(data, monYear);
              datasDisplayHisto(data, monYear);
              dataTotalTmaAnnuel(data, monYear);
              datasToDisplayContrat(data, monYear);
         }
      },
      error: function (resultat, statut, erreur) {
          console.log(resultat, statut, erreur);
      }
  });
}

function sqlTicketAgent(){
  $.ajax({
      url: '../model/queryTicketAgent.php', // La ressource ciblée
      type: 'GET', // Le type de la requête HTTP.
      data: {year: +monYear},
      dataType: 'json', // Le type de données à recevoir
      success: function (data, statut) { // success est toujours en place, bien sûr !
        console.log(data, statut);
         if (data) {
              datasToDisplayAgent(data, monYear);
         }
      },
      error: function (resultat, statut, erreur) {
          console.log(resultat, statut, erreur);
      }
  });
}

function sqlTicketOuvertAgent(){
  $.ajax({
      url: '../model/queryTicketOuvertAgent.php', // La ressource ciblée
      type: 'GET', // Le type de la requête HTTP.
      data: {year: +monYear},
      dataType: 'json', // Le type de données à recevoir
      success: function (data, statut) { // success est toujours en place, bien sûr !
        console.log(data, statut);
         if (data) {
              dataTicketOuvertAgent(data, monYear);
         }
      },
      error: function (resultat, statut, erreur) {
          console.log(resultat, statut, erreur);
      }
  });
}

function sqlTmaAgent(){
  $.ajax({
      url: '../model/queryTMAAgents.php', // La ressource ciblée
      type: 'GET', // Le type de la requête HTTP.
      data: {year: +monYear},
      dataType: 'json', // Le type de données à recevoir
      success: function (data, statut) { // success est toujours en place, bien sûr !
        console.log(data, statut);
         if (data) {
              dataTmaAgent(data, monYear);
         }
      },
      error: function (resultat, statut, erreur) {
          console.log(resultat, statut, erreur);
      }
  });
}

    $("#2016").click(function () {
      monYear = "2016";
      regroupSql();
    });
    $("#2017").click(function () {
      monYear = "2017";
      regroupSql();
    });
    //Pour ajouter une année ajoutez un boutton, modifier la variable monYear avec l'année correspondante puis appeller la fonction regroupSql() comme ci-dessus
});

var datasDisplayHisto = function (_data, _year) {
    //var myDatas = TmaClient.TmaByYearClient(_data, _year);
    //drawChartSuper(myDatas);
};

var datasToDisplaySocietes = function (_data, _year) {
    var myDatas = Tma.SocietesByMonth(_data, _year);
    drawChart(myDatas);
};

var datasToDisplayAgent = function (_data, _year) {
    var myDatas = TicketAgent.Ticket(_data, _year);
    drawChartAgent(myDatas);
};

var datasToDisplayContrat = function (_data, _year) {
    var myDatas = TmaContrat.ContratByMonth(_data, _year);
    drawChartContrat(myDatas);
};

var dataTotalTmaAnnuel = function (_data, _year){
    //var myDatas = TmaAll.TmaByYear(_data, _year);
    var myDatas = TmaClient.TmaByYearClientCamenbert(_data, _year);
    drawChartTmaCamenbert(myDatas);
};

var dataTmaAgent = function (_data, _year){
  //var myDatas = TmaAgent.TmaAgents(_data, _year);
  //drawChartTmaAgent(myDatas);
};

var dataTicketOuvertAgent = function (_data, _year) {
  var myDatas = TicketOuvertAgent.Ticket(_data, _year);
  drawChartTicketOuvertAgent(myDatas);
};

//Afficher les graphiques aprés l'année sélectionné
function visible(){
  Total = document.getElementById("monMessage");
  Total.style.visibility = "visible";
  Total.style.display = "block";
  Graph1 = document.getElementById("histo_Tma");
  Graph1.style.visibility = "visible";
  Graph1.style.display = "block";
  Graph2 = document.getElementById("agent_Tma");
  Graph2.style.visibility = "visible";
  Graph2.style.display = "block";
  Graph3 = document.getElementById("ticket_agent");
  Graph3.style.visibility = "visible";
  Graph3.style.display = "block";
  Graph4 = document.getElementById("contrat_Tma");
  Graph4.style.visibility = "visible";
  Graph4.style.display = "block";
}

//Graphique Tma par mois (sociétés)
function drawChart(datas) {
  if (datas instanceof Array) {
    var data = google.visualization.arrayToDataTable(datas);

    var options = {
        title: "TMA consommée par mois (sociétées)",
        height: 800,
        egend: { position: 'top', maxLines: 3 },
        bar: { groupWidth: '75%' },
        isStacked: true,
        hAxis:{
          title: 'TMA'
        },
        vAxis:{
          title: 'mois'
        }

    };
    var chart = new google.visualization.BarChart(document.getElementById("agent_Tma"));
    chart.draw(data, options);
  }
};
//Graphique Tma par année (sociétés)
function drawChartSuper(datas) {
  if (datas instanceof Array) {
    var data = google.visualization.arrayToDataTable(datas);

    var options = {
        title: "TMA annuel (sociétées)",
        height:800,
        egend: { position: 'top', maxLines: 3 },
        bar: { groupWidth: '75%' },
        //is3D: true,
        hAxis:{
          title: 'TMA'
        },
        vAxis:{
          title: 'année'
        }
    };
    var chart = new google.visualization.PieChart(document.getElementById("histo_Tma"));
    chart.draw(data, options);
  }
};
//Graphique Tickets fermée par semaine(agents)
function drawChartAgent(datas) {
  if (datas instanceof Array) {
    var data = google.visualization.arrayToDataTable(datas);

    var options = {
        title: "Tickets fermés par semaine (agent)",
        height:800,
        egend: { position: 'top', maxLines: 3 },
        bar: { groupWidth: '75%' },
        isStacked: true,
        hAxis:{
          title: 'Tickets fermés'
        },
        vAxis:{
          title: 'semaines'
        }
    };
    var chart = new google.visualization.BarChart(document.getElementById("ticket_agent"));
    chart.draw(data, options);
  }
};
//Graphique Tma par mois (contrats)
function drawChartContrat(datas) {
  if (datas instanceof Array) {
    var data = google.visualization.arrayToDataTable(datas);

    var options = {
        title: "TMA consommée par mois (contrats)",
        height:800,
        egend: { position: 'top', maxLines: 3 },
        bar: { groupWidth: '75%' },
        isStacked: true,
        hAxis:{
          title: 'TMA'
        },
        vAxis:{
          title: 'mois'
        }
    };
    var chart = new google.visualization.BarChart(document.getElementById("contrat_Tma"));
    chart.draw(data, options);
  }
};
//Graphique Tma soldées par année(agent)
function drawChartTmaAgent(datas) {
  if (datas instanceof Array) {
    var data = google.visualization.arrayToDataTable(datas);

    var options = {
        title: "Tma (agent)",
        height:800,
        egend: { position: 'top', maxLines: 3 },
        bar: { groupWidth: '75%' },
        isStacked: true,
        hAxis:{
          title: 'TMA'
        },
        vAxis:{
          title: 'mois'
        }
    };
    var chart = new google.visualization.BarChart(document.getElementById("tma_agent"));
    chart.draw(data, options);
  }
}
//Graphique Tickets ouvert par semaine(agents)
function drawChartTicketOuvertAgent(datas){
  if (datas instanceof Array) {
    var data = google.visualization.arrayToDataTable(datas);

    var options = {
        title: "Tickets ouverts par semaine (agent)",
        height:800,
        egend: { position: 'top', maxLines: 3 },
        bar: { groupWidth: '75%' },
        isStacked: true,
        hAxis:{
          title: 'Tickets ouverts'
        },
        vAxis:{
          title: 'semaines'
        }
    };
    var chart = new google.visualization.BarChart(document.getElementById("ticketOuv_agent"));
    chart.draw(data, options);
  }
}

function drawChartTmaCamenbert(datas){
  var data = google.visualization.arrayToDataTable(datas);
  var options = {
          title: 'TMA annuel',
          is3D: true,
          height:800
        };

        var chart = new google.visualization.PieChart(document.getElementById('histo_Tma'));
        chart.draw(data, options);

}

//Couleurs aléatoires
function colorRandom(){
  var random1 = Math.floor((Math.random() * 250) + 1);
  var random2 = Math.floor((Math.random() * 250) + 1);
  var random3 = Math.floor((Math.random() * 250) + 1);
  return "#"+(random1).toString(16)+(random2).toString(16)+(random3).toString(16);
}
