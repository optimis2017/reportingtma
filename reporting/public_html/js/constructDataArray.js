var FormattingDatas = {
    datas : [],
    year : 0,
    Init : function(_datas, _year){
        this.datas = _datas;
        this.year = _year + "";
    },
    TestDatas : function() {
        return (this.datas instanceof Array);
    },
    TestYear : function() {
        return (typeof this.year == "number");
    }
};

var BasesFunctions = Object.create(FormattingDatas);

BasesFunctions.SommeTma = function (self) {
  return self.datas.reduce(function (all,item,index){
      all += +item.qte;
    return all;
  },0.00);
};

BasesFunctions.SommeTmaByClient = function (self) {
  self.datas = self.datas.reduce(function (all,item,index){

    indexObj = all.reduce(function(obj, itemObj, indexO){
      if(itemObj.name == item[self.valORname]){
        obj = indexO;
      }
      return obj;
    }, null);

    if(indexObj == null){
          all.push(item);
    }else{
      all[indexObj].qte = +all[indexObj].qte + +item.qte;
    }

    return all;
  },[]);
};
// sort by week
// return datas Array
BasesFunctions.SortBySem = function(self) {
    self.datas = self.datas.sort(function (a, b) {
        return a.semaine - b.semaine;
    });
};
// sort by month
// return datas Array
BasesFunctions.SortByMonth = function(self) {
    self.datas = self.datas.sort(function (a, b) {
        return a.month - b.month;
    });
};
// get all the Societies for a year
// return an array
BasesFunctions.AllSocieties = function(self){
    self.societes = self.datas.reduce(function (all, item, index) {
        if (
        all.indexOf(item[self.valORname]) == -1
        && item.year === self.year
        && item[self.valORname] != null) {

            all.push(item[self.valORname]);

        };
        return all;
    }, []);
};
// get all the Contrats for a year
// return an array
BasesFunctions.AllContrats = function(self){
    self.societes = self.datas.reduce(function (all, item, index) {
        if (
        all.indexOf(item[self.valORname]) == -1
        && item.year === self.year
        && item[self.valORname] != null) {

            all.push(item[self.valORname]);

        };
        return all;
    }, []);
};
// get all the Agents for a year
// return an array
BasesFunctions.AllAgents = function(self){
    self.agents = self.datas.reduce(function (all, item, index) {
        if(all.indexOf(item[self.valORname]) == -1
        && item[self.valORname] != null){
        all.push(item[self.valORname]);
        }
        return all;
    }, []);
};

BasesFunctions.SommeContrat = function(self) {
  return self.datas.reduce(function (all,item,index){

    let indexObj = all.reduce(function(obj, itemObj, indexO){
      if(itemObj.month == item.month){
        if(itemObj.value == item[self.valORname]){
          obj = indexO;
        }
      }
      return obj;
    }, null);

    if(indexObj == null){
      //all.push(item);
          let newItem = {
            month: item.month,
            name: item.name,
            qte: item.qte,
            value: item.value,
            year: item.year
          }
          all.push(newItem);
    }else{
      all[indexObj].qte = +all[indexObj].qte + +item.qte;
    }

    return all;
  },[]);
};

var Tma = Object.create(BasesFunctions);
var TmaAll = Object.create(BasesFunctions);
var TmaClient = Object.create(BasesFunctions);
var TmaContrat = Object.create(BasesFunctions);
var Ticket = Object.create(BasesFunctions);
var TicketAgent = Object.create(BasesFunctions);
var TmaAgent = Object.create(BasesFunctions);
var TicketOuvertAgent = Object.create(BasesFunctions);

Ticket.TicketsAll = function(_data){
  this.Init(_data);
  var TotalTicket = this.datas[0]["count(ticket.ticket_id)"];
  document.getElementById('TotalTicket').innerHTML = TotalTicket;
}

TicketAgent.Ticket = function(_data, _year){
  this.valORname = "lastname";
  this.Init(_data, _year);
  this.SortBySem(this);
  this.AllAgents(this);

  self = this;

  var ticketBySemaine = [];
  for (semaine = 1; semaine <=52; semaine++)
  {
    var initTempObj = [];
    for(s=0; s < this.agents.length; s++)
    {
        initTempObj.push(0);
    }
    let laSemaine = "" + semaine;
    var tempObj = {};
    tempObj[laSemaine] = this.datas.reduce(function (all, item, index) {
            if (item.semaine === laSemaine) {
                all[self.agents.indexOf(item[self.valORname])] = +item.tickets;
            }
        return all;
    }, initTempObj);
    ticketBySemaine.push(tempObj);
  };
  var ArrayToReturn = [];
  var lineToReturn = [];

  lineToReturn = this.agents.reduce(function (all, item, index) {
      all.push(item);
      return all;
  }, ["Agents"]);

  ArrayToReturn.push(lineToReturn);

  ticketBySemaine.forEach(function (element) {
      var tempLine = [];
      for (var key in element) {
          tempLine = element[key].reduce(function (all, item, index) {
              all.push(item);
              return all;
          }, [key]);
      };
      ArrayToReturn.push(tempLine);
  });

  return ArrayToReturn;
}

TicketOuvertAgent.Ticket = function(_data, _year){
  this.valORname = "lastname";
  this.Init(_data, _year);
  this.SortBySem(this);
  this.AllAgents(this);

  self = this;

  var ticketBySemaine = [];
  for (semaine = 1; semaine <=52; semaine++)
  {
    var initTempObj = [];
    for(s=0; s < this.agents.length; s++)
    {
        initTempObj.push(0);
    }
    let laSemaine = "" + semaine;
    var tempObj = {};
    tempObj[laSemaine] = this.datas.reduce(function (all, item, index) {
            if (item.semaine === laSemaine) {
                all[self.agents.indexOf(item[self.valORname])] = +item.tickets;
            }
        return all;
    }, initTempObj);
    ticketBySemaine.push(tempObj);
  };
  var ArrayToReturn = [];
  var lineToReturn = [];

  lineToReturn = this.agents.reduce(function (all, item, index) {
      all.push(item);
      return all;
  }, ["Agents"]);

  ArrayToReturn.push(lineToReturn);

  ticketBySemaine.forEach(function (element) {
      var tempLine = [];
      for (var key in element) {
          tempLine = element[key].reduce(function (all, item, index) {
              all.push(item);
              return all;
          }, [key]);
      };
      ArrayToReturn.push(tempLine);
  });

  return ArrayToReturn;

}

/*TmaAgent.TmaAgents = function(_data, _year){
  this.valORname = "lastname";
  this.Init(_data, _year);
  this.AllAgents(this);

  self = this;

  var tmaByAgents = [];
  var initTempObj = [];
  for(s=0; s < this.agents.length; s++){
      initTempObj.push(0);
  }
  var tempObj = {};
  tempObj = this.datas.reduce(function (all, item, index) {
          all[self.agents.indexOf(item[self.valORname])] = +item.qte;
      return all;
  }, initTempObj);
  tmaByAgents.push(tempObj);
  var ArrayToReturn = [];
  var lineToReturn = [];

  lineToReturn = this.agents.reduce(function (all, item, index) {
      all.push(item);
      return all;
  }, ["Agents"]);

  ArrayToReturn.push(lineToReturn);

  tmaByAgents.forEach(function (element) {
      var tempLine = [];
      for (var key in element) {
          tempLine = element[key].reduce(function (all, item, index) {
              all.push(item);
              return all;
          }, [key]);
      };
      ArrayToReturn.push(tempLine);
  });

  return ArrayToReturn;
}*/

Tma.SocietesByMonth = function(_data, _year){
    this.valORname = "name";
    this.Init(_data, _year);
    this.SortByMonth(this);
    this.AllSocieties(this);

    self = this;

    // get qte by society for month : [{month1: [qte_socName1, qte_socName2, ...]}, ...]
    var mySocQteByMonth = [];
    for (mois = 1; mois <= 12; mois++) {
        //un tableau de X societes, avec toutes qte = 0
        var initTempObj = [];
        for (s = 0; s < this.societes.length; s++) {
            initTempObj.push("0");
        }

        let theMonth = "" + mois;
        var tempObj = {};
        tempObj[theMonth] = this.datas.reduce(function (all, item, index) {
            if (item.year === self.year) {
                if (item.month === theMonth) {
                    all[self.societes.indexOf(item[self.valORname])] = -item.qte;
                }
            }
            return all;
        }, initTempObj);
        mySocQteByMonth.push(tempObj);

        // [
        //     ['Genre', 'Fantasy & Sci Fi', 'Romance', 'Mystery/Crime', 'General', 'Western', 'Literature', { role: 'annotation' }],
        //     ['2010', 10, 24, 20, 32, 18, 5, ''],
        //     ['2020', 16, 22, 23, 30, 16, 9, ''],
        //     ['2030', 28, 19, 29, 30, 12, 13, '']
        // ]
    };

    var ArrayToReturn = [];
    var lineToReturn = [];

    lineToReturn = this.societes.reduce(function (all, item, index) {
        all.push(item);
        return all;
    }, ["Sociétés"]);
    ArrayToReturn.push(lineToReturn);

    mySocQteByMonth.forEach(function (element) {
        var tempLine = [];
        for (var key in element) {
            tempLine = element[key].reduce(function (all, item, index) {
                all.push(parseFloat(item));
                return all;
            }, [key]);
        };
        ArrayToReturn.push(tempLine);
    });

    return ArrayToReturn;
}

TmaAll.TmaByYear = function(_data, _year){
  this.Init(_data, _year);
  var TmaTotalAnnuel = this.SommeTma(this);
  document.getElementById('TotalTma').innerHTML = TmaTotalAnnuel;
}

/*TmaClient.TmaByYearClient = function(_data, _year){
  this.valORname = "name";
  this.Init(_data, _year);
  this.SommeTmaByClient(this);
  this.AllSocieties(this);

  self = this;

  // get qte by society
      //un tableau de X societes, avec toutes qte = 0
      var mySocQteByYear = [];
      var initTempObj = [];
      for (s = 0; s < this.societes.length; s++) {
          initTempObj.push("0");
      }

      let theYear = "" + _year;
      var tempObj = {};
      tempObj[theYear] = this.datas.reduce(function (all, item, index) {
          if (item.year === self.year) {
                  all[self.societes.indexOf(item[self.valORname])] = -item.qte;
          }
          return all;
      }, initTempObj);

      mySocQteByYear.push(tempObj);

  var ArrayToReturn = [];
  var lineToReturn = [];

  lineToReturn = this.societes.reduce(function (all, item, index) {
      all.push(item);
      return all;
  }, ["Sociétées"]);
  ArrayToReturn.push(lineToReturn);

  mySocQteByYear.forEach(function (element) {
      var tempLine = [];
      for (var key in element) {
          tempLine = element[key].reduce(function (all, item, index) {
              all.push(parseFloat(item));
              return all;
          }, [key]);
      };
      ArrayToReturn.push(tempLine);
  });
  console.log(ArrayToReturn);
  return ArrayToReturn;
}*/

TmaClient.TmaByYearClientCamenbert = function(_data, _year){
  this.valORname = "name";
  this.Init(_data, _year);
  this.SommeTmaByClient(this);
  this.AllSocieties(this);

  self = this;

  // get qte by society
      //un tableau de X societes, avec toutes qte = 0

  var ArrayToReturn = [];
  var lineToReturn = [];

  ArrayToReturn.push(["organisation","TMA"]);

  this.datas.reduce(function(all, item, index){
    all.push([item.name, -item.qte])
    return all;
  },ArrayToReturn);

  console.log(ArrayToReturn);
  return ArrayToReturn;
}

TmaContrat.ContratByMonth = function(_data, _year){
    this.valORname = "value";
    this.Init(_data, _year);
    this.SortByMonth(this);
    this.AllContrats(this);
    TmaContrat.data = this.SommeContrat(this);

    self = this;

    // get qte by society for month : [{month1: [qte_socName1, qte_socName2, ...]}, ...]
    var mySocQteByMonth = [];
    for (mois = 1; mois <= 12; mois++) {
        //un tableau de X societes, avec toutes qte = 0
        var initTempObj = [];
        for (s = 0; s < this.societes.length; s++) {
            initTempObj.push("0");
        }

        let theMonth = "" + mois;
        var tempObj = {};
        tempObj[theMonth] = this.data.reduce(function (all, item, index) {
            if (item.year === self.year) {
                if (item.month === theMonth) {
                    all[self.societes.indexOf(item[self.valORname])] = -item.qte;
                }
            }
            return all;
        }, initTempObj);

        mySocQteByMonth.push(tempObj);

        // [
        //     ['Genre', 'Fantasy & Sci Fi', 'Romance', 'Mystery/Crime', 'General', 'Western', 'Literature', { role: 'annotation' }],
        //     ['2010', 10, 24, 20, 32, 18, 5, ''],
        //     ['2020', 16, 22, 23, 30, 16, 9, ''],
        //     ['2030', 28, 19, 29, 30, 12, 13, '']
        // ]
    };

    var ArrayToReturn = [];
    var lineToReturn = [];

    lineToReturn = this.societes.reduce(function (all, item, index) {
        all.push(item);
        return all;
    }, ["Contrat"]);
    ArrayToReturn.push(lineToReturn);

    mySocQteByMonth.forEach(function (element) {
        var tempLine = [];
        for (var key in element) {
            tempLine = element[key].reduce(function (all, item, index) {
                all.push(parseFloat(item));
                return all;
            }, [key]);
        };
        ArrayToReturn.push(tempLine);
    });

    return ArrayToReturn;
}
