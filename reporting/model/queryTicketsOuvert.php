<?php
    require_once("connexionbdd.php");
    $pdo = new PDO('mysql:host='.DBHOST.';dbname='.DBNAME,DBUSER,DBPASS);
    $statement = $pdo->query("SELECT count(ticket.ticket_id) FROM ost_ticket AS ticket
    LEFT JOIN ost_ticket_status AS status
    ON status.id = ticket.status_id
    WHERE status.state='open' ;");
    $row = $statement->fetchAll(PDO::FETCH_ASSOC);
    header("content-type:application/json");
    echo json_encode($row);
    exit();
