<?php
    require_once("connexionbdd.php");
    $pdo = new PDO('mysql:host='.DBHOST.';dbname='.DBNAME,DBUSER,DBPASS);
    $statement = $pdo->query("SELECT firstname,lastname,count(ticket.ticket_id) AS tickets,ticket.created,WEEK(ticket.created) AS semaine
    FROM ost_staff as staff,ost_ticket AS ticket
    LEFT JOIN ost_ticket_status AS status
    ON status.id = ticket.status_id
    WHERE status.state='open'
    AND year(ticket.created) = ".$_GET['year']."
    AND staff.staff_id = ticket.staff_id
    GROUP BY firstname, semaine;");
    $row = $statement->fetchAll(PDO::FETCH_ASSOC);
    header("content-type:application/json");
    echo json_encode($row);
    exit();
