<?php
    require_once("connexionbdd.php");
    $pdo = new PDO('mysql:host='.DBHOST.';dbname='.DBNAME,DBUSER,DBPASS);
    $statement = $pdo->query("SELECT firstname,lastname,SUM(stock.quantite) as qte
    FROM ost_stock_tickets as stock
    LEFT JOIN ost_ticket as ticket
    ON ticket.ticket_id = stock.ticket_id
    LEFT JOIN ost_staff as staff
    ON staff.staff_id = ticket.staff_id
    WHERE  stock.quantite<0
    AND year(ticket.closed) = ".$_GET['year']."
    GROUP BY lastname;");
    $row = $statement->fetchAll(PDO::FETCH_ASSOC);
    header("content-type:application/json");
    echo json_encode($row);
    exit();
