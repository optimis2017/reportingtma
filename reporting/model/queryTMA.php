<?php
    require_once("connexionbdd.php");
    $pdo = new PDO('mysql:host='.DBHOST.';dbname='.DBNAME,DBUSER,DBPASS);
    $statement = $pdo->query("SELECT form.value ,SUM(stock.quantite) as qte,month(stock.created) as month,year(stock.created) as year,org.name
    FROM ost_form_entry_values as form
    LEFT JOIN ost_form_entry as entry
    ON entry.id = form.entry_id
    LEFT JOIN ost_organization as org
    ON org.id = entry.object_id
    LEFT JOIN ost_stock_tickets as stock
    ON stock.org_id = org.id
    AND stock.quantite < 0
    WHERE field_id = 34
    AND quantite is not null
    AND year(stock.created) = ".$_GET['year']."
    GROUP BY form.value
    ,org.name;");
    $row = $statement->fetchAll(PDO::FETCH_ASSOC);
    header("content-type:application/json");
    echo json_encode($row);
    exit();
