<?php
    // $pdo = new PDO('mysql:host=localhost;dbname=optimis', 'denis', '0000');
    require_once("connexionbdd.php");
    $pdo = new PDO('mysql:host='.DBHOST.';dbname='.DBNAME,DBUSER,DBPASS);
    $pdo->exec("SET NAMES 'utf8';");
    $statement = $pdo->query("SELECT firstname,lastname,count(ticket.ticket_id) AS tickets,closed,WEEK(closed) AS semaine
    FROM ost_staff as staff,ost_ticket AS ticket
    LEFT JOIN ost_ticket_status AS status
    ON status.id = ticket.status_id
    WHERE status.state='closed'
    AND staff.staff_id = ticket.staff_id
    AND year(closed) = ".$_GET['year']."
    GROUP BY firstname, semaine");
    $row = $statement->fetchAll(PDO::FETCH_ASSOC);
    header("content-type:application/json");
    echo (json_encode($row));
    exit();
