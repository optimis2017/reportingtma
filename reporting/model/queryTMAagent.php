<?php
    require_once("connexionbdd.php");
    $pdo = new PDO('mysql:host='.DBHOST.';dbname='.DBNAME,DBUSER,DBPASS);
    $statement = $pdo->query("SELECT firstname,lastname,SUM(quantite)
    FROM optimis.user as user, ost_stock_tickets as stock
    LEFT JOIN ost_ticket as ticket
    ON ticket.ticket_id = stock.ticket_id
    WHERE user.id = ticket.team_id
    AND year(stock.created) = ".$_GET['year']."
    GROUP BY lastname; ");
    $row = $statement->fetchAll(PDO::FETCH_ASSOC);
    header("content-type:application/json");
    echo json_encode($row);
    exit();
